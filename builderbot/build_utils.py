from typing import Union
from dataclasses import dataclass
import json

@dataclass
class BuildStatus:
    project_name: str
    status: str

    def __str__(self) -> str:
        output = ""
        if self.status in ["KNOWN",
                           "FETCHING",
                           "FETCHED",
                           "BUILDING",
                           "BUILT",
                           "TESTING",
                           "TESTED",
                           "DEPLOYING",
                           "DEPLOYED",
                           "SUCCESS"]:
            output = "\033[0;32m"
        else:
            output = "\033[0;31m"
        output += f"{self.project_name}: {self.status}"
        return output + "\033[0m"


@dataclass
class BuildOutput:
    project_name: str
    output: str

    def __str__(self) -> str:
        output = f"Output for: {self.project_name}\n"
        output += self.output
        return output
        


def parse_message(msg: str) -> Union[str, BuildStatus, BuildOutput]:
    data = json.loads(msg)
    if data["type"] == "event":
        return str(data["data"])
    if data["type"] == "build_output":
        return BuildOutput(
            data["data"]["project_name"],
            data["data"]["output"]
        )
    if data["type"] == "build_status":
        return BuildStatus(
            data["data"]["project"],
            data["data"]["status"]
        )
    return ""
