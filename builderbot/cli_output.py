from typing import Union
import os
os.system("")

from builderbot.build_utils import BuildOutput, BuildStatus

class CLIOutput:
    def __init__(self):
        self._build_statuses = {}
        self._last_num_update_lines = 1
        
    def update(self, status: Union[BuildStatus, BuildOutput, str]) -> None:
        self._clear_line()
        if isinstance(status, BuildStatus):
            self._build_statuses[status.project_name] = status
        elif isinstance(status, BuildOutput):
            print(str(status))
        else:
            print(status)
        self._print_status()

    def _clear_line(self):
        for _ in range(self._last_num_update_lines):
            print("\033[A\033[K",end="")

    def _print_status(self):
        print()
        for status in self._build_statuses.values():
            print(str(status))
        self._last_num_update_lines = len(self._build_statuses)+1

            
