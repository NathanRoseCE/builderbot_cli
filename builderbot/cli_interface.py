import asyncio
import argparse
import sys
import json
from typing import List
import websockets
from builderbot.build_utils import parse_message

from builderbot.cli_output import CLIOutput

def parse_args(args: List[str]):
    parser = argparse.ArgumentParser(
        prog="BuilderBot",
        description="An interface to submit requests to a build server"
    )
    parser.add_argument(
        "--remote", default="ws://localhost:54294"
    )
    sub_parsers = parser.add_subparsers(title="commands", dest="command")

    build_command = sub_parsers.add_parser(
        "release",
        help="Basic build command"
    )
    build_command.add_argument(
        "-r", "--repo",
        help="url of the repo"
    )
    build_command.add_argument(
        "-c", "--commit",
        help="url of the repo"
    )
    build_command = sub_parsers.add_parser(
        "clean",
        help="Basic build command"
    )
    config = parser.parse_args(args)
    _custom_validation(config)
    return config

def _custom_validation(config):
    if config.command == "release":
        if config.repo and config.commit:
            pass
        elif config.repo or config.commit:
            raise ValueError("must specify repo and commit")


async def main(args) -> None:
    config = parse_args(args)
    async with websockets.connect(config.remote) as websocket:
        output = CLIOutput()
        if config.command == "release":
            request = {
                "request": "release",
                "repo_url": config.repo,
                "commit": config.commit
            }
            print(f"submitting request: {request}")
            await websocket.send(json.dumps(request))
            while True:
                response = str(await websocket.recv())
                output.update(parse_message(response))
                if json.loads(response)["type"] == "status":
                    break
        if config.command == "clean":
            request = {
                "request": "clean"
            }
            print(f"submitting request: {request}")
            await websocket.send(json.dumps(request))
            response = await websocket.recv()
            print(f"response: {response}")
            
