import asyncio
import sys

from .cli_interface import main

if __name__ == "__main__":
    asyncio.run(main(args=sys.argv[1:]))
