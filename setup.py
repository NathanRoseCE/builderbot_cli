import setuptools

setuptools.setup(
    name="builderbot_cli",
    version="0.0.1",
    author="Nathan Rose",
    description="Builderbot cli args",
    packages=setuptools.find_packages(),
    tests_require=["pytest"],
    install_requires=[
        "pytest",
        "cryptography",
        "moto[all]==3.1.18",
        "boto3"
    ]
)
